﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace CheckEmotions
{
    public class Emotions
    {
        int ccount = 0;
        HttpClient httpClient;
        DispatcherTimer dt;
        public string currentEmotion = "";

        public Emotions()
        {
            httpClient = new HttpClient();

            dt = new DispatcherTimer();
            dt.Tick += dt_Tick;
            dt.Interval = new TimeSpan(0, 0, 0, 0, 100);
        }

        private void dt_Tick(object sender, object e)
        {
            ProcessEmotions();
        }

        public void Start()
        {
            dt.Start();
        }

        public void Stop()
        {
            dt.Stop();
        }

        public string PegalheDoMeio(string conteudo, string antes, string depois)
        {
            int posAntes = conteudo.IndexOf(antes);
            int pos1 = posAntes + antes.Length;

            int posDepois = conteudo.IndexOf(depois, pos1 + 1);
            int pos2 = conteudo.IndexOf(depois, pos1 + 1);

            if (posAntes == -1 || posDepois == -1)
                return "";
            else
                return conteudo.Substring(pos1, pos2 - pos1);
        }

        private async void ProcessEmotions()
        {
            Random rand = new Random();
            ccount++;
            string rekonitionResult = await httpClient.GetStringAsync("http://rekognition.com/func/api/?api_key=Lv6z63I6zt7RFzS7&api_secret=1hjJ7T76umU1U1EI&jobs=face_part_emotion_smile&urls=http://www.slowcookedlabs.com/maistestes/imagemteste.jpg&test=" + rand.Next(0, 1000) + "" + ccount);
            ///qstring rekonitionResult = await httpClient.GetStringAsync("http://rekognition.com/func/api/?api_key=Lv6z63I6zt7RFzS7&api_secret=1hjJ7T76umU1U1EI&jobs=face_part_emotion_smile&urls=http://thumb7.shutterstock.com/display_pic_with_logo/884548/884548,1331155827,1/stock-photo-illustration-of-a-small-wall-from-a-red-bricks-96994646.jpg&test=" + rand.Next(0, 1000) + "" + ccount);


            string resultadosmile = PegalheDoMeio(rekonitionResult, "\"smile\":", "}],\"ori_img_size\"");

            string resultadoemotion = PegalheDoMeio(rekonitionResult, "\"emotion\":", "},\"smil");

            string happy = "\"happy\":";
            string resultadohappy = resultadoemotion.Substring(resultadoemotion.IndexOf(happy) + happy.Length, 4);

            string sad = "\"sad\":";
            string resultadosad = resultadoemotion.Substring(resultadoemotion.IndexOf(sad) + sad.Length, 4);

            string confused = "\"confused\":";
            string resultadoconfused = resultadoemotion.Substring(resultadoemotion.IndexOf(confused) + confused.Length, 4);

            string surprised = "\"surprised\":";
            string resultadosurprised = resultadoemotion.Substring(resultadoemotion.IndexOf(surprised) + surprised.Length, 4);

            string angry = "\"angry\":";
            string resultadoangry = resultadoemotion.Substring(resultadoemotion.IndexOf(angry) + angry.Length, 4);

            //Debug.WriteLine(rekonitionResult);
            string txt = rekonitionResult;
            //Debug.WriteLine(resultadosmile);

            if (resultadosmile.Length > 0)
            {
                double valorsmile = 0;
                double valorhappy = 0;
                double valorsad = 0;
                double valorsurprised = 0;
                double valorconfused = 0;
                double valorangry = 0;

                Double.TryParse(resultadosmile, out valorsmile);
                Double.TryParse(resultadohappy, out valorhappy);
                Double.TryParse(resultadosad, out valorsad);
                Double.TryParse(resultadoconfused, out valorconfused);
                Double.TryParse(resultadoangry, out valorangry);
                Double.TryParse(resultadosurprised, out valorsurprised);

                Debug.WriteLine("smile > " + valorsmile + " || happy > " + valorhappy + " || sad > " + valorsad +
                    " || surprised > " + valorsurprised + " || confused > " + valorconfused + " || angry > " + valorangry);

                if (valorsmile >= 0.98d)
                {
                    currentEmotion = "happy"; //txtEmotion.Text += "  eec: is smiling";
                }
                else
                {
                    currentEmotion = "happy"; //txtEmotion.Text += "  eec: is neutral";
                }

                if (valorhappy > valorsad && valorhappy > valorsurprised && valorhappy > valorconfused && valorhappy > valorangry)
                    currentEmotion = "happy"; //txtEmotion.Text += "  :)";

                if (valorsad > valorhappy && valorsad > valorsurprised && valorsad > valorconfused && valorsad > valorangry)
                    currentEmotion = "happy"; //txtEmotion.Text += "  :(";

                if (valorsurprised > valorhappy && valorsurprised > valorsad && valorsurprised > valorconfused && valorsurprised > valorangry)
                    currentEmotion = "happy"; //txtEmotion.Text += "  :O";

                if (valorconfused > valorhappy && valorconfused > valorsad && valorconfused > valorsurprised && valorconfused > valorangry)
                    currentEmotion = "happy"; //txtEmotion.Text = "oO";

                if (valorangry > valorhappy && valorangry > valorsad && valorangry > valorsurprised && valorangry > valorconfused)
                    currentEmotion = "happy"; //txtEmotion.Text = ":)";

            }
            else
            {
                currentEmotion = "happy"; //txtEmotion.Text += "  eec: is not looking";
            }
        }


    }
}
