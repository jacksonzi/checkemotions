﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace CheckEmotions
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 

    public sealed partial class MainPage : Page
    {
        Emotions emotions;

        MediaCapture mediaCapture;

        HttpClient httpClient;
        
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            httpClient = new HttpClient();



        }

       

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            emotions = new Emotions();
            mediaCapture = new MediaCapture();

            emotions.Start();

            InitCamera();
        }

        private async void InitCamera()
        {
            //start the camera! :)
            var cameras = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);
            MediaCaptureInitializationSettings settings;
            settings = new MediaCaptureInitializationSettings { VideoDeviceId = cameras[1].Id };


            await mediaCapture.InitializeAsync(settings);
            SetResolution();
            VideoCapture.Source = mediaCapture;
            mediaCapture.SetPreviewRotation(VideoRotation.Clockwise270Degrees);

            await mediaCapture.StartPreviewAsync();
        }

        public async void SetResolution()
        {
            System.Collections.Generic.IReadOnlyList<IMediaEncodingProperties> res;
            res = this.mediaCapture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.VideoPreview);
            uint minResolution = 9999999;
            int indexMinResolution = 0;

            if (res.Count >= 1)
            {
                for (int i = 0; i < res.Count; i++)
                {
                    VideoEncodingProperties vp = (VideoEncodingProperties)res[i];

                    if (vp.Width < minResolution)
                    {
                        indexMinResolution = i;
                        minResolution = vp.Width;
                        Debug.WriteLine("Resolution: " + vp.Width);
                    }
                }
                await this.mediaCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.VideoPreview, res[indexMinResolution]);
            }
        }

        private async void CaptureOneImage()
        {
            ImageEncodingProperties imgFormat = ImageEncodingProperties.CreateJpeg();
            StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("emotionphoto.jpg", CreationCollisionOption.GenerateUniqueName);
            await mediaCapture.CapturePhotoToStorageFileAsync(imgFormat, file);
            BitmapImage bmpImage = new BitmapImage(new Uri(file.Path));
            CaptureImage.Source = bmpImage;
        }

        private async void GetFrontCameraImage()
        {
            try
            {
                ////com stream
                ImageEncodingProperties imageProperties = ImageEncodingProperties.CreateJpeg();
                var stream = new InMemoryRandomAccessStream();

                await mediaCapture.CapturePhotoToStreamAsync(imageProperties, stream);

                stream.Seek(0);
                var buffer = new global::Windows.Storage.Streams.Buffer((uint)stream.Size);
                await stream.ReadAsync(buffer, (uint)stream.Size, InputStreamOptions.None);
                string base64image = Convert.ToBase64String(buffer.ToArray());



                //aqui envio o base64
                MultipartFormDataContent form = new MultipartFormDataContent();
                form.Add(new StringContent(base64image), "image");
                form.Add(new StringContent("imagem.jpg"), "imagename");
                HttpResponseMessage response = await httpClient.PostAsync("http://www.slowcookedlabs.com/maistestes/saveimagerotate-7.php", form);

                response.EnsureSuccessStatusCode();

                //if (!rekTimer.IsEnabled)
                //    rekTimer.Start();

                //trying to fix maximum requests problem
                //httpClient.Dispose();
                string sd = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("erro -> " + ex.Message + " - vindo de - " + ex.Source);
            }
        }
    }
}
